
user: admin
pass: usuario123456

user: el_cesar06
pass: usuario123456

--------------------------------------------

Hola me llamo cesar y este es mi segundo proyecto esta basado en un framework de php orientado a objetos con JQuery

Esta es una pagina de criptomonedas.

---------------------------------------------

HOME

La pagina HOME tiene un carrusel y debajo un buscador que puedes buscar cualquier criptomoneda
que te llevara a unos detalles sobre la misma recogidos mediante una api.
Debajo, encontramos categorias de criptomonedas ordenadas por las mas visitadas, con un boton debajo
que nos permite visualizar mas categorias.

----------------------------------------------

LIST

Cuando pulsamos una categoria en el HOME, recogemos de BD las monedas guardadas y las pintamos por mas visitadas, debajo de estas criptomonedas tenemos un pagination que nos permite visualizar mas criptomonedas ocultas.Tambien se guarda en BD que ha sido visitada dicha categoria.
Debajo de todas estas criptomonedas tenemos un mapa el cual pinta una ubicacion que esta guardada en BD.

----------------------------------------------

DETAILS

Una vez pulsamos una criptomoneda del list anterior, podemos encontrar los detalles sobre dicha criptomoneda. Tambien se guarda en BD que ha sido visitada dicha criptomoneda y dicha categoria.

---------------------------------------------

SHOP

Shop seria basicamente lo mismo que el list pero con todas las criptomonedas, aqui no hay mapa ya que al haber 60 criptomonedas no queria saturar el mapa. Una vez pulsamos una criptomoneda seria igual que el details anterior.Tambien se guarda en BD que ha sido visitada dicha criptomoneda y dicha categoria.


--------------------------------------------

Create user

Este es un modulo creado inicialmente para crear usuarios con Dependent Drop Dawnns recogidos de api o de fichero, dependiendo de si falla el internet, si entramos podremos ver que no funciona, esto se debe a tema de rutas no solucionadas, pero el codigo esta ahi.

--------------------------------------------

CONTACT

Tenemos para seleccionar el motivo de la consulta y luego escribimos la consulta, una vez lo enviamos si todo ha ido bien un toastr nos notificara que todo ha ido bien, y recibiremos al correo especificado una copia de la consulta.

--------------------------------------------

PROFILE

Este modulo solo esta disponible para usuarios registrados, nos muestra un poco de informacion sobre el usuario y esta la podemos cambiar, si sale todo bien un toastr nos lo notificara.

--------------------------------------------

LOGIN

Las contraseñas del login se cifran con SHA256 para ser comparadas con BD, el register guarda la contraseña en SHA256.
En este modulo, podemos visualizar un apartado de login, otro de register y otro de singin con social media. Si nos registramos recibiremos un correo para activar nuestra cuenta con un token y con esto sabemos que usuario es, si nos logeamos, y todo ha ido bien nos notificara con un toastr y seremos redireccionados al home, tambien se generara un token y serà actualizado en BD,si el usuario tiene un avatar serà mostrado si no, tendra uno predeterminado.
Con el singin social si cuando nos logeamos frente a google o twiter, si el usuario no se encuentra en BD serà registrado, si no, serà logeado, actualizando un token generado aleatoriamente
Tambien podemos hacer un recover password, pulsamos en el "he olvidado mi contraseña" y seremos redireccionados a un formulario de correo y contraseña, ponemos nuestro correo, nuestra contraseña nueva y recibiremos un correo con un link con un token para validar el cambio, el cambio de contraseña (el update, se realiza cuando el usuario pulsa en el link, dentro de esa funcion, recoge el token y la contraseña estara guardada en $_SESSION para ahora poder guardarla en BD).
Tambien, cuando nos logeamos aparece en el menu, aparte de PROFILE, aparece LogOut y login desaparece, ya que si el usuario ya esta logeado no tiene necesidad de ir al login, cuando le damos al LOGOUT, nos redirecciona al login, y quita del menu PROFILE Y LOGOUT y pone el login, tambien elimina del localstorage el token que se habia guardado cuando nos hemos logeado (este token cuando nos logeamos se guarda ya sea por singin social o login normal)