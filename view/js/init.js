$(document).ready(function() {
    toastr.options.timeOut = 3000; // 1.5s
    toastr.info('Page Loaded!');

    /* toastr.success('Se ha enviado un correo a tu e-mail');*/

    document.getElementById('log_out').addEventListener('click', function() {
        document.getElementById('log_out').style.display = 'none';
        localStorage.removeItem('log_user');
        $.ajax({
          type: "POST",
          url: "../../login/log_out",
          success: function(data){
            var data = JSON.parse(data);
            $("#usuario").empty();
            $("#usuario").append("Usuario: Guest");
            toastr.success(data[0]);
            setTimeout(
            function() 
            {
              window.location.replace("../../login/login/");
            }, 1000);
          }
        });
    })
});
