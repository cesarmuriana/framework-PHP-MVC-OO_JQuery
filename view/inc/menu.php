<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <input type='image' src=<?php  echo $_SESSION['imagen'] ?> id='avatar' height='50px' width='50px'>
        </div>
        <?php 
            if (!isset($_GET['module'])) {
                $_GET['module']="home";
            }
        ?>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class=  
                    <?php
                    if(($_GET['module']=="home")&&($_GET['function']=='index')){
                        echo "active";
                    }else{ 
                        echo "";
                    }?>
                    >
                <a href="<?php amigable('?module=home&function=index'); ?>">Home</a></li>
                <li class=  
                    <?php
                    if(($_GET['module']=="home")&&($_GET['function']=='shop')){
                        echo "active";
                    }else{ 
                        echo "";
                    }?>
                    >
                <a href="<?php amigable('?module=home&function=shop'); ?>">Shop</a></li>                 
                <li class= 
                    <?php
                    if($_GET['module']=="usuario"){
                        echo "active";
                    }else{ 
                        echo "";
                    }?>
                    >
                <a href="<?php amigable('?module=usuario&function=create_user'); ?>">Create User</a></li>
                <li class=  
                    <?php
                    if($_GET['module']=="login"){
                        echo "active";
                    }else{ 
                        echo "";
                    }?>
                    >
                <a <?php 
                    if($_SESSION['login']==='hidden'){ 
                        ?> style="display:none" <?php } ?> 
                id="menu_login"
                href="<?php amigable('?module=login&function=login'); ?>">Login</a></li>
                <li class=  
                    <?php
                    if($_GET['module']=="contact"){
                        echo "active";
                    }else{ 
                        echo "";
                    }?>
                    >
                <a href="<?php amigable('?module=contact&function=contact'); ?>">Contact</a></li>
                <li>
                </li>
                <li>
                <a id="usuario">Usuario:
                    <?php
                    if($_SESSION['usuario']){
                        echo $_SESSION['usuario'];
                    }else{ 
                        echo "Guest";
                    }?>
                </a></li>
                <li class=  
                    <?php
                    if($_GET['module']=="profile"){
                        echo "active";
                    }else{ 
                        echo "";
                    }
                    if ($_SESSION['usuario'] === 'Guest'){
                    ?>     hidden <?php }

                    ?>
                    >
                <a href="<?php amigable('?module=profile&function=profile'); ?>">Profile</a></li>
                <li>
                <a><button id="log_out"
                <?php
                if ($_SESSION['usuario'] === 'Guest'){
                ?>     hidden <?php } ?>
                >Log Out</button>
                </a></li>
            </ul>
        </div>
    </div>
</div>
