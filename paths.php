<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define('SITE_ROOT', $path);

//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/programacio/FW-PHP-OO-JQuery/');

//CSS
define('CSS_PATH', SITE_PATH . 'view/css/');

//JS
define('JS_PATH', SITE_PATH . 'view/js/');

//IMG
define('IMG_PATH', SITE_PATH . 'view/img/');
/* Plugins */
define('PLUGINS_PATH', SITE_PATH . 'view/plugins/');


define('PRODUCTION', true);

/* model */
define('MODEL_PATH', SITE_ROOT . 'model/');
/* view */
define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');
define('VIEW_PATH_INC_ERROR', SITE_ROOT . 'view/inc/templates_error/');
/* modules */
define('MODULES_PATH', SITE_ROOT . 'modules/');
/* resources */
define('RESOURCES', SITE_ROOT . 'resources/');
/* media */
define('MEDIA_PATH', SITE_ROOT . 'media/');
/* utils */
define('UTILS', SITE_ROOT . 'utils/');

/* model usuario */
define('FUNCTIONS_usuario', SITE_ROOT . 'modules/usuario/');
define('MODEL_PATH_usuario', SITE_ROOT . 'modules/usuario/model/');
define('DAO_usuario', SITE_ROOT . 'modules/usuario/model/DAO/');
define('BLL_usuario', SITE_ROOT . 'modules/usuario/model/BLL/');
define('VIEW_USUARIO', SITE_ROOT . 'modules/usuario/view/');
define('MODEL_usuario', SITE_ROOT . 'modules/usuario/model/model/');
define('USUARIO_JS_PATH', SITE_ROOT . 'modules/usuario/view/js/');
define('UTILS_usuario', SITE_ROOT . 'modules/utils/');

/* model home */
define('UTILS_HOME', SITE_ROOT . 'modules/home/');
/* define('PRODUCTS_JS_LIB_PATH', SITE_ROOT . 'modules/home/view/lib/'); */
define('HOME_JS_PATH', '../../modules/home/view/js/');
define('MODEL_PATH_HOME', SITE_ROOT . 'modules/home/model/');
define('DAO_HOME', SITE_ROOT . 'modules/home/model/DAO/');
define('BLL_HOME', SITE_ROOT . 'modules/home/model/BLL/');
define('VIEW_HOME', SITE_ROOT . 'modules/home/view/');
define('MODEL_HOME', SITE_ROOT . 'modules/home/model/model/');
define('FUNCTIONS_HOME', SITE_ROOT . 'modules/home/resources/');
define('HOME_VIEW', SITE_PATH . 'home/index/');

/* model contact */
define('UTILS_CONTACT', SITE_ROOT . 'modules/contact/');
define('CONTACT_JS_PATH', '../../modules/contact/view/js/');
define('VIEW_CONTACT', SITE_ROOT . 'modules/contact/view/');
define('MODEL_CONTACT', SITE_ROOT . 'modules/contact/model/model/');
define('FUNCTIONS_CONTACT', SITE_ROOT . 'modules/contact/resources/');

/* model login  */
define('UTILS_LOGIN', SITE_ROOT . 'modules/login/');
define('LOGIN_JS_PATH', '../../modules/login/view/js/');
define('MODEL_PATH_LOGIN', SITE_ROOT . 'modules/login/model/');
define('DAO_LOGIN', SITE_ROOT . 'modules/login/model/DAO/');
define('BLL_LOGIN', SITE_ROOT . 'modules/login/model/BLL/');
define('VIEW_LOGIN', SITE_ROOT . 'modules/login/view/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');
define('FUNCTIONS_LOGIN', SITE_ROOT . 'modules/login/resources/');

/* model profile  */
define('UTILS_PROFILE', SITE_ROOT . 'modules/profile/');
define('PROFILE_JS_PATH', '../../modules/profile/view/js/');
define('MODEL_PATH_PROFILE', SITE_ROOT . 'modules/profile/model/');
define('DAO_PROFILE', SITE_ROOT . 'modules/profile/model/DAO/');
define('BLL_PROFILE', SITE_ROOT . 'modules/profile/model/BLL/');
define('VIEW_PROFILE', SITE_ROOT . 'modules/profile/view/');
define('MODEL_PROFILE', SITE_ROOT . 'modules/profile/model/model/');
define('FUNCTIONS_PROFILE', SITE_ROOT . 'modules/profile/resources/');



/* amigables */
define('URL_AMIGABLES', TRUE);
