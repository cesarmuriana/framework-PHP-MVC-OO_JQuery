

//Solution to : "Uncaught Error: Dropzone already attached."
Dropzone.autoDiscover = false;
$(document).ready(function () {
    //console.log("Inside ready");

    $( "#date_reception" ).datepicker({
        dateFormat: 'dd/mm/yy',
        //dateFormat: 'mm-dd-yy',
        changeMonth: true, changeYear: true,
        minDate: -90, maxDate: "+1M"
    });
    $( "#date_expiration" ).datepicker({
      dateFormat: 'dd/mm/yy',
      //dateFormat: 'mm-dd-yy',
      changeMonth: true, changeYear: true,
      minDate: 0, maxDate: "+36M"
    });


    $('#submit_user').click(function(){
        //console.log("Inside click function");
        //console.log($('input[name="packaging"]:checked').val());
        validate_user();
    });

   

    //Dropzone function //////////////////////////////////
    $("#dropzone").dropzone({
        url: "../../usuario/upload",
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "An error has occurred on the server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
                console.log(file.name);
                console.log("Response: "+response);
            });
        },
        complete: function (file) {
            //if(file.status == "success"){
            //alert("El archivo se ha subido correctamente: " + file.name);
            //}
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            console.log(name);
            $.ajax({
                type: "POST",
                url: "../../usuario/delete",
                data: "filename=" + name,
                success: function (data) {
                  //console.log(name);
                  console.log(data);
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    //console.log(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) !== null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            return false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element2;
                        if ((element2 = file.previewElement) !== null) {
                            element2.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    }

                }
            });
        }
    });//End dropzone

    var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
    //var val_dates = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
    var val_dates = /\d{2}.\d{2}.\d{4}$/;
    var normal_string = /^[0-9a-zA-Z]{2,20}$/;
    var prod_price = /(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;
    var string_description = /^(.){1,500}$/;
    //var string_description = /^[0-9A-Za-z]{2,90}$/;

    /* Fade out function  to hide the error messages */

    $("#nameuser").keyup(function () {
      if ($(this).val() !== "" && normal_string.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#username").keyup(function () {
      if ($(this).val() !== "" && normal_string.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#surname").keyup(function () {
      if ($(this).val() !== "" && normal_string.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#referraluser").keyup(function () {
      if ($(this).val() !== "" && prod_price.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });

    $("#proddesc").keyup(function () {
      if ($(this).val() !== "" && string_description.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
    });
    
    /* Ejecucion Dependent combos */ 
    load_countries_v1();


  function validate_user(){
      var result = true;

      var nameuser = document.getElementById('nameuser').value;
      var username = document.getElementById('username').value;
      var surname = document.getElementById('surname').value;
      var referraluser = document.getElementById('referraluser').value;
      var date_reception = document.getElementById('date_reception').value;
      var date_expiration = document.getElementById('date_expiration').value;
      var interesado = false;
      var sexo = document.getElementById('sexo').value;

      var Bitcoin = document.getElementById('Bitcoin').checked;
      var IOTA = document.getElementById('IOTA').checked;
      var Litecoin = document.getElementById('Litecoin').checked;
      var Ethereum = document.getElementById('Ethereum').checked;

      if ((Bitcoin === true) || (IOTA === true) || (Litecoin === true) || (Ethereum === true)){
        interesado=true;
      };

      var country = document.getElementById('country').value;
      var province = document.getElementById('province').value;
      var city = document.getElementById('city').value;
      var proddesc = document.getElementById('proddesc').value;

      var username_val = /^[0-9a-zA-Z]+[\-_'\s]?[0-9a-zA-Z]+$/;
      var string_out_space_low_bar = /^[ 0-9a-zA-Z ]+[\-_'\s ]?[0-9a-zA-Z ]+$/;
      var string_out_space = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z]+$/;
      var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
      //var val_dates = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
      var val_dates = /\d{2}.\d{2}.\d{4}$/;
      var out_space = /^[0-9a-zA-Z]{2,20}$/;
      var prod_price = /(?=.)^\$?(([1-9][0-9]{0,2}(,[0-9]{3})*)|[0-9]+)?(\.[0-9]{1,2})?$/;
      var string_description = /^(.){1,500}$/;


      $(".error").remove();
      if ($("#nameuser").val() === "" || $("#nameuser").val() === "Input name"){
        $("#nameuser").focus().after("<span class='error'>Input user name</span>");
        return false;
      }else if(!username_val.test($("#nameuser").val())){
        $("#nameuser").focus().after("<span class='error'>User name must be 2 to 30 letters without spaces</span>");
        return false;
      }

      $(".error").remove();
      if ($("#username").val() === "" || $("#username").val() === "Input name"){
        $("#username").focus().after("<span class='error'>Input name</span>");
        return false;
      }else if(!string_out_space.test($("#username").val())){
        $("#username").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
        return false;
      }

      if ($("#surname").val() === "" || $("#surname").val() === "Input surname") {
          $("#surname").focus().after("<span class='error'>Input surname</span>");
          return false;
      } else if (!string_reg.test($("#surname").val())) {
          $("#surname").focus().after("<span class='error'>Surname must be 2 to 30 letters</span>");
          return false;
      }

      if ($("#referraluser").val() === "" || $("#referraluser").val() === "Input referral") {
          $("#referraluser").focus().after("<span class='error'>Input referral</span>");
          return false;
      } else if (!string_out_space_low_bar.test($("#referraluser").val())) {
          $("#referraluser").focus().after("<span class='error'>Referral must be 2 to 30 letters</span>");
          return false;
      }

      if ($("#date_reception").val() === "" || $("#date_reception").val() === "Input reception date") {
          $("#date_reception").focus().after("<span class='error'>JS Input product reception date</span>");
          return false;
      } else if (!val_dates.test($("#date_reception").val())) {
          $("#date_reception").focus().after("<span class='error'>JS Input product reception date</span>");
          return false;
      }

      if ($("#date_expiration").val() === "" || $("#date_expiration").val() === "Input expiration date") {
          $("#date_expiration").focus().after("<span class='error'>JS Input product expiration date</span>");
          return false;
      } else if (!val_dates.test($("#date_expiration").val())) {
          $("#date_expiration").focus().after("<span class='error'>JS Input product expiration date</span>");
          return false;
      }

      if ($("#country").val() === "" || $("#country").val() === "Select country" || $("#country").val() === null) {
          $("#country").focus().after("<span class='error'>Select one country</span>");
          return false;
      }

      if ($("#province").val() === "" || $("#province").val() === "Select province") {
          $("#province").focus().after("<span class='error'>Select one province</span>");
          return false;
      }

      if ($("#city").val() === "" || $("#city").val() === "Select city") {
          $("#city").focus().after("<span class='error'>Select one city</span>");
          return false;
      }

      if ($("#proddesc").val() === "" || $("#proddesc").val() === "Input product description") {
          $("#proddesc").focus().after("<span class='error'>Input product description</span>");
          return false;
      } else if (!string_description.test($("#proddesc").val())) {
          $("#proddesc").focus().after("<span class='error'>Description cannot be empty</span>");
          return false;
      }
      
      //console.log("Before if result");
      if (result){
        //console.log("Inside if result");

          if (province === null) {
              province = 'default_province';
          }else if (province.length === 0) {
              province = 'default_province';
          }else if (province === 'Select province') {
              return 'default_province';
          }

          if (city === null) {
              city = 'default_city';
          }else if (city.length === 0) {
              city = 'default_city';
          }else if (city === 'Select city') {
              return 'default_city';
          }

        var data = {
              "nameuser": nameuser,
              "username": username,
              "surname": surname, 
              "referraluser": referraluser, 
              "date_expiration": date_expiration,
              "date_reception": date_reception, 
              "interesado": interesado,
              "Bitcoin": Bitcoin,
              "IOTA": IOTA,
              "Litecoin": Litecoin,
              "Ethereum": Ethereum,
              "sexo": sexo, 
              "country": country, 
              "province": province, 
              "city": city, 
              "proddesc": proddesc
            };
        
        var data_user_JSON = JSON.stringify(data);
        var alta_usuario_json = true;

        $.ajax({
          type: "POST",
          url: "../../usuario/alta_usuario_json",
          data: {data_user_JSON},
          success: function (data) {

            if (data === "minium_range")
              $("#date_expiration").focus().after("<span class='error'>Minium days 4</span>");
            
            if (data === "username")
              $("#nameuser").focus().after("<span class='error'>This username is already exist</span>");

            if (data === "referal_user")
              $("#referraluser").focus().after("<span class='error'>This referal not exist</span>");
            
            if (data === "low_date")
              $("#date_reception").focus().after("<span class='error'>The date can not be today or less than today</span>");

            if (data === "hig_date")
              $("#date_expiration").focus().after("<span class='error'>The expiration date can not be less than the initial date</span>");
            
            try{
              var data =JSON.parse(data);
              var error = data['error'];
              if ( error === true ){
                alert(data['mensaje']);
              }else{
                window.location.replace(data['redirect']);
                alert("Usuario agregado con exito");
              }
            }catch(err){
              alert("Error inesperado");
            }
          }
        });
    }//End if result
  }//End validate_product

/*----------http://country.io/data/----------*/

  function load_countries_v1() {
    $.ajax({
        type: "GET",
        url: "https://api.printful.com/countries",
        success: function (data) {
          var data = data.result;
            $.each(data, function (i, valor) {
                $("#country").append("<option value='" + valor.code + "'>" + valor.name + "</option>");
            });
        },
        error: function () {
            $.ajax({
                type: "POST",
                url: "../../usuario/load_country",
                data: {load_country},
                success: function (data) {
                  $.getJSON( data, function( data ) {
                    $.each(data, function (i, valor) {
                      $("#country").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
                    });
                  });
                },
                error: function () {
                  alert("Error on load countries");
                }
            });
        }
    });
  }

/* ------------------------------ LOAD_PROVINCES ------------------------------*/

  $("#country").change(function(){
    var eleccion = document.getElementById("country").value;
    if ( eleccion === 'ES'){
      $.ajax({
          type: "GET",
          url: "https://api.printful.com/countriesasd",
          success: function (data) {
            /* No se encontro ninguna api (asi que se simula que el servidor no responde) */ 
          },
          error: function () {
            $.ajax({
                type: "POST",
                url: "../../usuario/load_provinces",
                success: function (data) {
                  try {
                    var provincias = JSON.parse(data);
                    $.each(provincias, function (i, valor) {
                      $("#province").append("<option value='" + valor.idprovincia + "'>" + valor.provinciaseo + "</option>");
                    });
                  }catch(err) {
                    alert("Error al cargar las provincias");
                  }
                },
                error: function () {
                  alert("Error al encontrar las provincias");
                }
            });
          }
      });
    }else{
      /*$('.province').attr('disabled');*/
      $('#province').removeAttr('disabled');
      $('#city').removeAttr('disabled');
      alert("No tenemos acceso a provincias que no sean españolas");
    }
  });


  /* ------------------------------ LOAD_CITIES ------------------------------*/

  $("#province").change(function(){
    var eleccion = document.getElementById("province").value;
      $.ajax({
          type: "GET",
          url: "https://api.printful.com/countriesasd",
          success: function (data) {
            /* No se encontro ninguna api (asi que se simula que el servidor no responde) */
          },
          error: function () {
            var load_poblacion = "true";
            $.ajax({
                type: "POST",
                url: "../../usuario/load_poblacion",
                data:{eleccion},
                success: function (data) {
                  try {
                    var ciudades = JSON.parse(data);
                    $.each(ciudades, function (i, valor) {
                      $("#city").append("<option value='" + valor.idpoblacion + "'>" + valor.poblacionseo + "</option>");
                    });
                  }catch(err) {
                    alert("Error al cargar las ciudades");
                  }
                },
                error: function () {
                  alert("Error al encontrar las ciudades");
                }
            });
          }
      });
  });

});//End document ready