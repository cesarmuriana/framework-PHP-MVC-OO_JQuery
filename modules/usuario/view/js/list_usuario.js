$(document).ready(function () {
    var get_user=true;
    $.ajax({
        type: "POST",
        url: "modules/usuario/controller/controller_usuario.class.php",
        data: {get_user},
        success: function (data) {

            var user = JSON.parse(data);
            var interesado = [];
            if ( user.interesado == 1 ){
                if ( user.Bitcoin == 1 )
                    interesado.push("Bitcoin");;

                if ( user.IOTA == 1 )
                    interesado.push("IOTA");

                if ( user.Litecoin == 1 )
                    interesado.push("Litecoin");
                
                if ( user.Ethereum == 1 )
                    interesado.push("Ethereum");
            }else{
                interesado.push("No hay ninguna preferencia");
            }

            var usuario =   "<div id='datos_usuario'>"+
                            "<p id='alias_user'>Alias de usuario: "+ user.nameuser+"</p>"+
                            "<p id='nombre_user'>Nombre de usuario: "+ user.username+"</p>"+
                            "<p id='apellido_user'>Apellido de usuario: "+user.surname +"</p>"+
                            "<p id='referal_user'>Referal user: "+ user.referraluser+"</p>"+
                            "<p id='date_rep_user'>Date reception: "+user.date_reception +"</p>"+
                            "<p id='date_exp_user'>Date expiration: "+user.date_expiration +"</p>"+
                            "<p id='interesado_user'>Intereses: "+interesado+"</p>"+
                            "<p id='sexo_user'>Sexo: "+user.sexo +"</p>"+
                            "<p id='country_user'>Codigo de pais: "+user.country +"</p>"+
                            "<p id='province_user'>Codigo de provincia: "+user.province +"</p>"+
                            "<p id='city_user'>Codigo ciudad: "+user.city +"</p>"+
                            "<p id='proddesc_user'>Descripcion del usuario: "+user.proddesc +"</p>"+
                            "</div>";
            $("#input-user").append(usuario);
        }
    });
});