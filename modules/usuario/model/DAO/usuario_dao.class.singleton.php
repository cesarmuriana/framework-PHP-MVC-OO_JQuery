<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class usuario_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_user_dao($db, $usuario) {

        $nameuser = $usuario['nameuser'];
        $username = $usuario['username'];
        $surname = $usuario['surname'];
        $referraluser = $usuario['referraluser'];
        $date_expiration = $usuario['date_expiration'];
        $date_reception = $usuario['date_reception'];
        $interesado = $usuario['interesado'];
        $sexo = $usuario['sexo'];
        $country = $usuario['country'];
        $province = $usuario['province'];
        $city = $usuario['city'];
        $proddesc = $usuario['proddesc'];

        if (!empty($usuario['Bitcoin'])){
            $Bitcoin=$usuario['Bitcoin'];
        }else{
            $Bitcoin="0";
        }

        if (!empty($usuario['IOTA'])){
            $IOTA=$usuario['IOTA'];
        }else{
            $IOTA="0";
        }

        if (!empty($usuario['Litecoin'])){
            $Litecoin=$usuario['Litecoin'];
        }else{
            $Litecoin="0";
        }

        if (!empty($usuario['Ethereum'])){
            $Ethereum=$usuario['Ethereum'];
        }else{
            $Ethereum="0";
        }

        $sql = "INSERT INTO `FW_OO_PHP`.`usuario` (`nameuser`,`username`,`surname`,`referraluser`,`date_reception`,`date_expiration`,`interesado`,`sexo`,`country`,`province`,`userdesc`,`img`,`Bitcoin`,`IOTA`,`Litecoin`,`Ethereum`) VALUES ('$nameuser','$username','$surname','$referraluser','$date_reception','$date_expiration','$interesado','$sexo','$country','$province','$proddesc','Una img','$Bitcoin','$IOTA','$Litecoin','$Ethereum')";

        return $db->ejecutar($sql);
    }

    public function get_provinces_dao($db){

      $sql="SELECT idprovincia,provinciaseo from provincia";
      
      return $db->listar($sql);

    }

    public function get_cities_dao($db, $id_province){

        $sql="SELECT idpoblacion,poblacionseo from poblacion where idprovincia=$id_province";

        return $db->listar($sql);
    }

    public function search_user_dao($db, $user){

        $sql="SELECT * from usuario where nameuser='$user'";

        return $db->listar($sql);
    }

}//End productDAO
