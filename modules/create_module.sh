#!/bin/bash
read -p "Nombre del nuevo modulo: " nombre
mkdir $nombre
cd ./$nombre
mkdir "controller" "model" "resources" "utils" "view"
touch ./controller/controller_"$nombre".class.php
cd "model"
mkdir "BLL" "DAO" "model"
cd ..
touch ./model/BLL/"$nombre"_bll.class.singleton.php
touch ./model/DAO/"$nombre"_dao.class.singleton.php
touch ./model/model/"$nombre"_model.class.singleton.php
touch ./resources/functions.xml
cd ./view
mkdir js
touch "$nombre".html
touch ./js/controller_"$nombre".js
