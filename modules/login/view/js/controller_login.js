$(document).ready(function() {
  // Initialize Firebase

  var config = {
    apiKey: "AIzaSyBSlHjADOfB8aawG17uryFvRNiwBJwVF64",
    authDomain: "fw-php-oo-jquery.firebaseapp.com",
    databaseURL: "https://fw-php-oo-jquery.firebaseio.com",
    projectId: "fw-php-oo-jquery",
    storageBucket: "fw-php-oo-jquery.appspot.com",
    messagingSenderId: "715470256652"
  };
  firebase.initializeApp(config);

  /* ---- GOOGLE --------- */  

      $("#logingoogle").click(function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('email');
      
          var authService = firebase.auth();

          // manejador de eventos para loguearse
          document.getElementById('logingoogle').addEventListener('click', function() {
            authService.signInWithPopup(provider)
                  .then(function(result) {
                      var usuario = [];
                      var name = result.additionalUserInfo.profile.given_name;
                      var username = result.user.displayName;
                      var email = result.user.email;
                      var img = result.user.photoURL;
                      var uid = result.user.uid;
                      usuario.push(name);
                      usuario.push(username);
                      usuario.push(email);
                      usuario.push(img);
                      usuario.push(uid);
                      $.ajax({
                        type: "POST",
                        url: "../../login/login_google",
                        data: {usuario},
                        success: function(data){
                          try{
                            console.log(data);
                            var data = JSON.parse(data);
                            localStorage.log_user = JSON.stringify(data[0]);
                            var user = data[2];
                            toastr.success(data[1]);
                            $("#usuario").empty();
                            $("#usuario").append("Usuario: "+user);
                            document.getElementById('menu_login').style.display = 'none';
                            setTimeout(
                            function() 
                            {
                              window.location.replace("../../home/index/");
                            }, 1000);
                          }catch(err){
                            toastr.success("Error login");
                          }
                          
                        }
                      });
                      document.getElementById('log_out').style.display = 'block';
                  })
                  .catch(function(error) {
                      console.log('Se ha encontrado un error:', error);
                  });
          })
          
         
      });
/* -------------- TWITER ---------- */
  $("#logintwitter").click(function() {
      var provider = new firebase.auth.TwitterAuthProvider();
      var authService = firebase.auth();
  
      document.getElementById('logintwitter').addEventListener('click', function() {
          authService.signInWithPopup(provider).then(function(result) {
              var usuario = [];
              var username = result.user.displayName;
              var img = result.user.photoURL;
              var uid = result.user.uid;
              var name = '';
              var email = '';
              usuario.push(name);
              usuario.push(username);
              usuario.push(email);
              usuario.push(img);
              usuario.push(uid);
              $.ajax({
                type: "POST",
                url: "../../login/login_twiter",
                data: {usuario},
                success: function(data){
                  var data = JSON.parse(data);
                  localStorage.log_user = JSON.stringify(data[0]);
                  var user = data[2];
                  toastr.success(data[1]);
                  $("#usuario").empty();
                  $("#usuario").append("Usuario: "+user);
                  document.getElementById('menu_login').style.display = 'none';
                  setTimeout(
                  function() 
                  {
                    window.location.replace("../../home/index/");
                  }, 1000);
                }
              });
              document.getElementById('log_out').style.display = 'block';
          }).catch(function(error) {
            //var errorCode = error.code;
            //var errorMessage = error.message;
            //var email = error.email;
            //var credential = error.credential;
          });
      })
  });

/* Login and register js */

    $('#pass_login').keypress(function(e) {
        if(e.which == 13) {
            validate_user_login();
        }
    });
    $('#usuario_login').keypress(function(e) {
        if(e.which == 13) {
            validate_user_login();
        }
    });
    $('#pass_register').keypress(function(e) {
        if(e.which == 13) {
            validate_register();
        }
    });
    $('#pass_reset2').keypress(function(e) {
        if(e.which == 13) {
            validate_reset();
        }
    });

    document.getElementById('login_btn').addEventListener('click', function() {
      validate_user_login();
    });
    document.getElementById('register_btn').addEventListener('click', function() {
      validate_register();
    });

});

    function validate_user_login(){

        if ((document.login.usuario_login.value.length===0)){
            $("#mensaje_login").focus().after("<span class='error'>Tiene que escribir su usuario</span>");
            document.login.usuario_login.focus();
            return ;
            }
            document.getElementById('usuario_login').innerHTML = ""

        if(!document.login.usuario_login.value.match(/[a-z]/)){
            $("#mensaje_login").focus().after("<span class='error'>Solo letras por favor</span>");
            document.login.usuario_login.focus();
            return 0;
            }
        document.getElementById('usuario_login').innerHTML = ""
        if (document.login.pass_login.value.length===0){
          $("#mensaje_pass_login").focus().after("<span class='error'>Tiene que escribir su password</span>");
            document.login.pass_login.focus();
            return 0;
        }
        document.getElementById('pass_login').innerHTML = ""
        
        var usuario = [];

        var user = document.getElementById('usuario_login').value;
        var pass = document.getElementById('pass_login').value;

        usuario.push(user);
        usuario.push(pass);

        $.ajax({
          type: "POST",
          url: "../../login/login_own",
          data: {usuario},
          success: function(data){
            try{
              var data = JSON.parse(data);
              var user = data[2];
              if (data[1]==='Login succefull'){
                toastr.success(data[1]);
                localStorage.log_user = JSON.stringify(data[0]);
                $("#usuario").empty();
                $("#usuario").append("Usuario: "+user);
                document.getElementById('log_out').style.display = 'block';
                document.getElementById('menu_login').style.display = 'none';
                setTimeout(
                function() 
                {
                  window.location.replace("../../home/index/");
                }, 1000);
              }else{
                toastr.error("Combrueba el usuario y la contraseña");
              }
            }catch(err){
              toastr.error("Combrueba el usuario y la contraseña");
            }
          }
        });


    };

    function validate_register(){

      if (document.register_user.usuario_register.value.length===0){
        $("#mensaje_usuario_reg").focus().after("<span class='error'>Tiene que escribir su usuario</span>");
      document.register_user.usuario_register.focus();
      return 0;
      }
      
      if (document.register_user.email_register.value.length===0){
        $("#mensaje_email_reg").focus().after("<span class='error'>Tiene que escribir su email</span>");
          document.register_user.email_register.focus();
          return 0;
      }
      
      if (document.register_user.pass_register.value.length===0){
        $("#mensaje_pass_reg").focus().after("<span class='error'>Tiene que escribir su password</span>");
          document.register_user.pass_register.focus();
          return 0;
      }

        var usuario = [];
        var user = document.getElementById('usuario_register').value;
        var email = document.getElementById('email_register').value;
        var pass = document.getElementById('pass_register').value;

        usuario.push(user);
        usuario.push(email);
        usuario.push(pass);
    
        $.ajax({
              type: "POST",
              url: "../../login/register_own",
              data: {usuario},
              success: function(data){
                try{
                  var data = JSON.parse(data);
                  var user = data[1];
                  if (data[0]==="El usuario ya existe"){
                    toastr.error(data[0]);
                    $("#mensaje_usuario_reg").focus().after("<span class='error'>Cambie el usuario</span>");
                    document.register_user.usuario_register.focus();
                  }else{
                    if(data[0]==="El e-mail ya existe"){
                        toastr.error(data[0]);
                        $("#mensaje_email_reg").focus().after("<span class='error'>Cambie el e-mail</span>");
                        document.register_user.email_register.focus();
                    }else{
                      toastr.success(data[0]);
                    }
                  }
                }catch(err){
                  toastr.error("Ha habido un error inesperado en el registro");
                  //window.location.replace("../../home/index/");
                }
              }
            });
    };


/*
    function cojepass() {
        var pass=document.recover.pass_reset2.value;
        return pass;
    }
    function cojemail() {
        var email=document.recover.email_reset.value;
        return email;
    }
    function setrecover() {
        var recover='si';
        return recover;
    }
    function validate_reset(){
    if (document.recover.email_reset.value.length===0){
      $("#e_email").focus().after("<span class='error'>Tiene que escribir su email</span>");
    document.recover.email_reset.focus();
    return 0;
    }
    document.getElementById('e_email').innerHTML = "";
    
    if (document.recover.pass_reset.value.length===0){
      $("#e_password").focus().after("<span class='error'>Escriba la nueva contraseña</span>");
        document.recover.pass_reset.focus();
        return 0;
    }
    document.getElementById('e_password').innerHTML = "";
    
    if (document.recover.pass_reset2.value.length===0){
      $("#e_password2").focus().after("<span class='error'>Tiene que repetir la contraseña</span>");
        document.recover.pass_reset2.focus();
        return 0;
    }
    document.getElementById('e_password2').innerHTML = "";

    if (document.recover.pass_reset2.value.length!==document.recover.pass_reset.value.length){
      $("#e_password2").focus().after("<span class='error'>Las contraseñas no coinciden</span>");
        document.recover.pass_reset2.focus();
        return 0;
    }
    document.getElementById('e_password2').innerHTML = "";
    prueba=document.recover.pass_reset2.value;

        $.ajax({
        type: "POST",
        url: "../../login/reset",
        data:
            {
            pass:cojepass(),
            email:cojemail(),
            recover:setrecover()
            },                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        success: function(data){
            if (data==='<script language="javascript">alert("Cambio de clave realizado con exito")</script><script>window.location.href="index.php?page=controller_user&op=list";</script>'){
                alert("Contraseña actualizada");
            }else{
                if(data.email == 'undefined'){ 
                }else{ 
                    var obj = JSON.parse(data);
                    $("#e_email").html(obj.email); }
                }
            }
        });
   // document.recover.submit();
   // document.recover.action="index.php?page=controller_users";
    };
    */