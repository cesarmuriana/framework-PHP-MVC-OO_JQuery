<?php
class controller_login{
        function __construct() {
            //include(UTILS . "common.inc.php");
        }

        function login(){

                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_LOGIN . 'login.html');

                require_once(VIEW_PATH_INC . "footer.html");
        }

        function login_google(){
            $retorno=[];
            $busqueda=[];

            $usuario = $_POST;

            $uid=$usuario['usuario'][4];
            $user=$usuario['usuario'][1];
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($retorno,$token);
            array_push($usuario['usuario'], $token);
            $exist_user=$usuario['usuario'][4];
            array_push($busqueda, "idusuario");
            array_push($busqueda, $exist_user);
            $usuario['usuario']= [];
            array_push($usuario['usuario'], $uid);
            array_push($usuario['usuario'], $token);

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            if ($finduser){
                $login = loadModel(MODEL_LOGIN, "login_model", "login_user", $usuario);
                if ($login){
                    array_push($retorno,"Login succefull");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Error on login");
                }
            }else{
                $usuario = $_POST;
                $uid=$usuario['usuario'][4];
                $user=$usuario['usuario'][1];
                $token = sha1(mt_rand(1, 90000) . 'SALT');
                array_push($usuario['usuario'], $token);
                array_push($usuario['usuario'], "");
                array_push($usuario['usuario'], "1");
                $save = loadModel(MODEL_LOGIN, "login_model", "save_login", $usuario);
                if ($save){
                    array_push($retorno,"Registro correcto");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Ha habido un error en el registro del usuario");
                }
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }           
        function login_twiter(){
            $retorno=[];
            $busqueda=[];
            
            $usuario = $_POST;
            $user=$usuario['usuario'][1];
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($retorno,$token);
            array_push($usuario['usuario'], $token);
            $exist_user=$usuario['usuario'][4];
            array_push($busqueda, "idusuario");
            array_push($busqueda, $exist_user);

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            if ($finduser){
                $login = loadModel(MODEL_LOGIN, "login_model", "login_user", $usuario);
                if ($login){
                    array_push($retorno,"Login succefull");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Error on login");
                }
            }else{
                $save = loadModel(MODEL_LOGIN, "login_model", "save_login", $usuario);
                if ($save){
                    array_push($retorno,"Registro correcto");
                    $_SESSION['usuario']=$user;
                    $_SESSION['imagen']=$finduser[0]['img'];
                    $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Ha habido un error en el registro del usuario");
                }
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }

        function login_own(){

            $retorno=[];
            $busqueda=[];
            
            $usuario = $_POST;
            $user=$usuario['usuario'][0];
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($retorno,$token);
            array_push($usuario['usuario'], $token);
            $exist_user=$usuario['usuario'][0];
            $password=$usuario['usuario'][1];
            $passwrd = hash("sha256" , $password);
            array_push($busqueda, "pass='$passwrd' and activate=1 and username");
            array_push($busqueda, $exist_user);
            $user = $usuario['usuario'][0];

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            if ($finduser){
                $usuario['usuario'] = [];
                array_push($usuario['usuario'],$user);
                array_push($usuario['usuario'],$token);
                $login = loadModel(MODEL_LOGIN, "login_model", "login_user", $usuario);
                if ($login){
                    array_push($retorno,"Login succefull");
                    $_SESSION['usuario']=$usuario['usuario'][0];
                    if ($finduser[0]['img']!='')
                        $_SESSION['imagen']=$finduser[0]['img'];
                        $_SESSION['login']="hidden";
                }else{
                    array_push($retorno,"Error on login");
                }
            }else{
                array_push($retorno,"La contraseña o el usuario es incorrecto, puede que tampoco haya validado su email");
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }
        
        function register_own(){
            $retorno=[];
            $busqueda=[];

            $usuario = $_POST;
            $user=$usuario['usuario'][0];
            $email=$usuario['usuario'][1];
            $passwrd=$usuario['usuario'][2];
            $passwrd = hash("sha256" , $passwrd);
            array_push($usuario['usuario'],$passwrd);
            $token = sha1(mt_rand(1, 90000) . 'SALT');
            array_push($busqueda, "nameuser");
            array_push($busqueda, $user);

            $finduser = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            
            if (!$finduser){
                $busqueda=[];
                array_push($busqueda, "email");
                array_push($busqueda, $email);
                $findemail = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
                if ($findemail){
                    array_push($retorno, "El e-mail ya existe");
                }else{
                    $usuario['usuario'] = [];
                    array_push($usuario['usuario'],$user);
                    array_push($usuario['usuario'],"");
                    array_push($usuario['usuario'],$email);
                    array_push($usuario['usuario'],"");
                    array_push($usuario['usuario'],$user);
                    array_push($usuario['usuario'],$token);
                    array_push($usuario['usuario'],$passwrd);
                    array_push($usuario['usuario'],"0");
                    $save = loadModel(MODEL_LOGIN, "login_model", "save_login", $usuario);
                    if ($save){
                        array_push($retorno, "Revise su correo");

                        $link ="http://127.0.0.1/programacio/FW-PHP-OO-JQuery/login/active_account/&token=$token";

                        $config = array();
                        $config['api_key'] = "key-5a273cbca614afd30b20de3fa94ef5f7"; //API Key
                        $config['api_url'] = "https://api.mailgun.net/v3/sandbox4e700072ff9c4da68f91d061b8bd0d51.mailgun.org/messages"; //API Base URL

                        $message = array();
                        $message['from'] = "cesar@equipocrypto.com";
                        $message['to'] = $email;
                        $message['h:Reply-To'] = "$email";
                        $message['subject'] = "Activacion cuenta";
                        $message['html'] = 'Hola ' . $email . ',<br></br>Active su cuenta pulsando en el link mostrado:<br></br>' . $link;
                     
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $config['api_url']);
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_POST, true); 
                        curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
                        $result = curl_exec($ch);
                        curl_close($ch);
                    }else{
                        array_push($retorno, "Ha habido un error en el registro");
                    }
                }
            }else{
                array_push($retorno, "El usuario ya existe");
            }
            array_push($retorno, $user);
            echo json_encode($retorno);
        }


        function log_out(){
            $retorno=[];
            $_SESSION['usuario']="Guest";
            $_SESSION['login']="";
            $_SESSION['imagen']=IMG_PATH."login.png";
            $mensaje="LogOut succefull";
            array_push($retorno,$mensaje,$_SESSION['imagen']);
            echo json_encode($retorno);

        }

        function active_account(){
                $token=$_GET['token'];

                $activate = loadModel(MODEL_LOGIN, "login_model", "activate", $token);
                if ($activate){
                    print_r("Su cuenta ha sido activada");
                }else{
                    print_r("Ha habdido un error en la activacion de la cuenta");
                }
                
        }

        function reset(){
                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_LOGIN . 'reset.html');

                require_once(VIEW_PATH_INC . "footer.html");
        }

        function reset_account(){
            $retorno = [];
            $busqueda = [];
            $email=$_POST['usuario'][0];
            $pass=$_POST['usuario'][1];
            $passwrd = hash("sha256" , $pass);
            $_SESSION['pass']=$passwrd;
            array_push($busqueda, "email");
            array_push($busqueda, $email);

            $findemail = loadModel(MODEL_LOGIN, "login_model", "search_user", $busqueda);
            $id=$findemail[0]['idusuario'];
            if ($findemail){
                $usuario['usuario'] = [];

                $token = sha1(mt_rand(1, 90000) . 'SALT');
                array_push($usuario['usuario'], $id);
                array_push($usuario['usuario'], $token);

                $save_token = loadModel(MODEL_LOGIN, "login_model", "login_user", $usuario);
                if ($save_token){
                    array_push($retorno, "Revise su correo");
                    $link ="http://127.0.0.1/programacio/FW-PHP-OO-JQuery/login/active_reset/&token=$token";


                    $config = array();
                    $config['api_key'] = "key-5a273cbca614afd30b20de3fa94ef5f7"; //API Key
                    $config['api_url'] = "https://api.mailgun.net/v3/sandbox4e700072ff9c4da68f91d061b8bd0d51.mailgun.org/messages"; //API Base URL

                    $message = array();
                    $message['from'] = "cesar@equipocrypto.com";
                    $message['to'] = $email;
                    $message['h:Reply-To'] = "$email";
                    $message['subject'] = "Cambio contraseña";
                    $message['html'] = 'Hola ' . $email . ',<br></br>Pulse en el siguiente link para cambiar la contraseña:<br></br>' . $link;
                 
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $config['api_url']);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_POST, true); 
                    curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
                    $result = curl_exec($ch);
                    curl_close($ch);
                }
            }

            echo json_encode($retorno);
        }
        function active_reset(){
                $usuario['usuario'] = [];

                $token=$_GET['token'];
                $pass=$_SESSION['pass'];

                array_push($usuario, $pass);
                array_push($usuario, $token);


                $activate = loadModel(MODEL_LOGIN, "login_model", "reset_pass", $usuario);
                if ($activate){
                    print_r("Su cuenta ha cambiado de contraseña");
                }else{
                    print_r("Ha habdido un error en el cambio de contraseña");
                }
                
        }

}