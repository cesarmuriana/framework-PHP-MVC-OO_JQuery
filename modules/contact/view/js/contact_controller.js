$(document).ready(function() {

	$( "#enviar" ).click(function() {
	  	if (document.getElementById("e_mail").value.length===3)
		  	$("#e_mail").focus().after("<span class='error'>Escriba su correo</span>");
		if (document.getElementById("contact_text").value.length===10)
		  	$("#contact_text").focus().after("<span class='error'>Cuentenos un poco sobre su consulta</span>");
		var email = document.getElementById("e_mail").value;
		var opcion = document.getElementById("inputSubject").value;
		var texto = document.getElementById("contact_text").value;
		$.ajax({
        type: "POST",
        url: "../../contact/send_contact",
        data: {email,opcion,texto},
        success: function (data) {
       		if (data == 'true'){
            toastr.success('Su consulta ha sido enviada con exito');
       		}else{
       			toastr.error('Ha habido un error al envio de su consulta');
       		}
        }
    });
	});
});