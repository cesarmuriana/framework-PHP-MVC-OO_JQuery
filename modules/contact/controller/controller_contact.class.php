<?php
class controller_contact{
        function __construct() {
            //include(UTILS . "common.inc.php");
        }

        function contact(){

                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_CONTACT . 'contact.html');

                require_once(VIEW_PATH_INC . "footer.html");
            }
        function send_contact(){

                $email=$_POST['email'];
                $opcion=$_POST['opcion'];
                $texto=$_POST['texto'];

                $config = array();
                $config['api_key'] = "key-5a273cbca614afd30b20de3fa94ef5f7"; //API Key
                $config['api_url'] = "https://api.mailgun.net/v3/sandbox4e700072ff9c4da68f91d061b8bd0d51.mailgun.org/messages"; //API Base URL

                $message = array();
                $message['from'] = "cesar@equipocrypto.com";
                $message['to'] = $email;
                $message['h:Reply-To'] = "$email";
                $message['subject'] = "$opcion";
                $message['html'] = 'Hola ' . $email . ',</br></br>Este es un mensaje generado automaticamente para que usted tenga constancia de esta consulta<br></br>Info consulta:<br></br>'. $texto;
             
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $config['api_url']);
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_POST, true); 
                curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
                $result = curl_exec($ch);
                curl_close($ch);
                echo json_encode(true);

                /*$json = send_mailgun('cesarmuriana1@gmail.com');
                print_r($json); */
            }
            
            
    }