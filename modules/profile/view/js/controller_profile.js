
$(document).ready(function() {
	
	getuser();

});

function getuser(){
	var token = localStorage.log_user;
	$.ajax({
	    type: "POST",
	    url: "../../profile/get_user",
	    data: {token},
	    success: function (data) {
	   		var data = JSON.parse(data);
	   		if (data == 'Error al recoger usuario'){
	   			toastr.success(data);
	   		}else{
	            var usuario = 	"<table>"+
	            				"<tr><td>User name:</td><td id='error_nameuser'><input type='text' name='nameuser' id='nameuser' required='required' placeholder='Name user' value='"+data[0].nameuser +"'/></td></tr>"+
	            				"<tr><td>Name: </td><td id='error_username'><input type='text' name='username' id='username' required='required' placeholder='User name' value='" + data[0].username +"'</td></tr>"+
	            				"<tr><td>Surname: </td><td id='error_surname'><input type='text' name='surname' id='surname' required='required' placeholder='User surname' value='"+data[0].surname+"'</td></tr>"+
	            				"<tr><td>Datebirthay: </td><td id='error_datebirthay'><input type='text' name='datebirthay' id='datebirthay' required='required' placeholder='Reception date' value='"+data[0].datebirthay+"'</td></tr>"+
	            				"<tr><td>Email: </td><td id='error_email'><input type='text' name='email' id='email' required='required' placeholder='email' value='" + data[0].email +"'</td></tr>"+
	            				"</table>"+
	            				"<button type='button' value='profile' id='submit_profile'>Submit</button>";
	            $("#profile").append(usuario);
           	    $('#submit_profile').click(function(){
			   		submit();
			    });
	   		}
	   		
	    }
	});


}
function submit(){
	var token = localStorage.log_user;
	var usuario = [];
	var user = document.getElementById('nameuser').value;
	var username = document.getElementById('username').value;
	var surname = document.getElementById('surname').value;
	var datebirthay = document.getElementById('datebirthay').value;
	var email = document.getElementById('email').value;

	usuario.push(token);
	usuario.push(user);
	usuario.push(username);
	usuario.push(surname);
	usuario.push(datebirthay);
	usuario.push(email);

    $.ajax({
	    type: "POST",
	    url: "../../profile/save_user",
	    data: {usuario},
	    success: function(data){
	    	console.log(data);
	    	var data = JSON.parse(data);
	    	if (data == 'Usuario actualizado con exito'){
	    		toastr.success(data);
	    	}else{
	    		toastr.error("Error al actualizar el usuario");
	    	}
	    }
	});
}