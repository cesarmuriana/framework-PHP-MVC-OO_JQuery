<?php
class controller_profile{
        function __construct() {
            //include(UTILS . "common.inc.php");
        }

        function profile(){

                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_PROFILE . 'profile.html');

                require_once(VIEW_PATH_INC . "footer.html");
        }

        function get_user(){
        	$busqueda=[];
        	$token=$_POST['token'];
 			array_push($busqueda, "token");
            array_push($busqueda, $token);

 			$finduser = loadModel(MODEL_PROFILE, "profile_model", "search_user", $busqueda);
            if ($finduser){
                echo json_encode($finduser);
            }else{
                echo json_encode("Error al recoger usuario");
            }


        }
        function save_user(){
            
            $usuario=$_POST['usuario'];

            $save = loadModel(MODEL_PROFILE, "profile_model", "save_user", $usuario);
            if($save){
                echo json_encode("Usuario actualizado con exito");
            }else{
                echo json_encode("Error al actualizar");
            }


        }
}