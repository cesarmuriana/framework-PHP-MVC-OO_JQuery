<?php
//echo json_encode("products_bll.class.singleton.php");
//exit;

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define(SITE_ROOT, $path);
define('MODEL_PATH', SITE_ROOT . 'model/');

require(MODEL_PATH . "Db.class.singleton.php");
require(SITE_ROOT . "modules/home/model/DAO/home_dao.class.singleton.php");

class home_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = home_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function get_categories_dll(){
      return $this->dao->get_categories_dao($this->db);
    }

    public function get_monedas_dll($data){
      return $this->dao->get_monedas_dao($this->db,$data);
    }

    public function get_busqueda_dll($busqueda){
      return $this->dao->get_busqueda_dao($this->db,$busqueda);
    }

    public function get_moneda_dll($busqueda){
      return $this->dao->get_moneda_dao($this->db,$busqueda);
    }

    public function popularity_cat_dll($category){
      return $this->dao->popularity_cat_dao($this->db,$category);
    }

    public function popularity_mon_dll($moneda){
      return $this->dao->popularity_mon_dao($this->db,$moneda);
    }

    public function count_total_mon_by_cat_dll($categoria){
      return $this->dao->count_total_mon_by_cat_dao($this->db,$categoria);
    }
    public function count_total_mon_bll(){
      return $this->dao->count_total_mon_dao($this->db);
    }
    public function get_all_monedas_dll($data){
      return $this->dao->get_all_monedas_dao($this->db,$data);
    }
    public function get_monedas_by_cat_dll($data){
      return $this->dao->get_monedas_by_cat_dao($this->db,$data);
    }
}