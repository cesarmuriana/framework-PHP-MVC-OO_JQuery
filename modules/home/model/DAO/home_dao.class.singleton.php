<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

/*  Recoger 5 primeras monedas mas populares 
select nombre from monedas where generacion=1 group by popularidad desc limit 5; */


class home_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function get_categories_dao($db){

        $sql="SELECT nombre,url,nombre_salida,cod_cat FROM categoria order by popularidad desc";
        return $db->listar($sql);

    }

    public function get_monedas_dao($db,$data){

        $generacion = $data['clase'];
        $num = $data['num'];
        $num=($num-1);
        $num=($num*4);

        $sql="SELECT nombre,nombre_web from monedas where generacion=$generacion order by popularidad desc limit $num,4";
        return $db->listar($sql);

    }
    public function get_all_monedas_dao($db,$data){

        $num = $data['num'];
        $num=($num-1);
        $num=($num*4);

        $sql="SELECT nombre,nombre_web from monedas order by popularidad desc limit $num,4";
        return $db->listar($sql);
    }
    
    public function get_monedas_by_cat_dao($db,$data){

        $sql="SELECT * from monedas where generacion=$data";
        return $db->listar($sql);

    }

    public function get_busqueda_dao($db,$busqueda){

        $sql="SELECT nombre_web,nombre from monedas where nombre like '%$busqueda%'";
        return $db->listar($sql);
    }

    public function get_moneda_dao($db,$busqueda){

        $sql="SELECT * from monedas where nombre_web='$busqueda'";
        return $db->listar($sql);
    }

    public function popularity_cat_dao($db,$category){

        $sql="UPDATE categoria set popularidad=popularidad+1 where cod_cat='$category'";
        return $db->ejecutar($sql);
    }

    public function popularity_mon_dao($db,$moneda){

        $sql="UPDATE monedas set popularidad=popularidad+1 where nombre_web='$moneda'";
        return $db->ejecutar($sql);
    }

    public function count_total_mon_by_cat_dao($db,$categoria){

        $sql="SELECT count(*) as 'total' FROM monedas where generacion=$categoria";
        return $db->listar($sql);
    }

    public function count_total_mon_dao($db){

        $sql="SELECT count(*) as 'total' FROM monedas";
        return $db->listar($sql);
    }

}//End productDAO