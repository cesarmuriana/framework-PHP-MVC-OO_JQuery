<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/FW-PHP-OO-JQuery/';
define('SITE_ROOT', $path);

require( SITE_ROOT . 'modules/home/model/BLL/home_bll.class.singleton.php');

class home_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = home_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function get_categories(){
        return $this->bll->get_categories_dll();
    }
    public function get_monedas($data){
        return $this->bll->get_monedas_dll($data);
    }
    public function get_busqueda($busqueda){
        return $this->bll->get_busqueda_dll($busqueda);
    }
    public function get_moneda($busqueda){
        return $this->bll->get_moneda_dll($busqueda);
    }
    public function popularity_cat($category){
        return $this->bll->popularity_cat_dll($category);
    }
    public function popularity_mon($moneda){
        return $this->bll->popularity_mon_dll($moneda);
    }
    public function count_total_mon_by_cat($category){
        return $this->bll->count_total_mon_by_cat_dll($category);
    }
    public function count_total_mon(){
        return $this->bll->count_total_mon_bll($category);
    }
    public function get_all_monedas($data){
        return $this->bll->get_all_monedas_dll($data);
    }
    public function get_monedas_by_cat($data){
        return $this->bll->get_monedas_by_cat_dll($data);
    }
}

