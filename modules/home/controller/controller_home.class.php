<?php

//session_start();

    /*if (!isset($_POST['op']))
        $_POST['op']='index';*/

class controller_home{
        function __construct() {
            //include(UTILS . "common.inc.php");
        }

        function index(){
                //echo "<script type='text/javascript'>alert('Hasta aqui llega');</script>";
                //$callback="index.php?module=home&view=home";
                //die('<script>window.location.href="'.$callback .'";</script>');

                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_HOME . 'home.html');

                require_once(VIEW_PATH_INC . "footer.html");
            }
        function categoria(){
                //echo "<script type='text/javascript'>alert('Hasta aqui llega');</script>";
                //$callback="index.php?module=home&view=home";
                //die('<script>window.location.href="'.$callback .'";</script>');

                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_HOME . 'list_result.html');

                require_once(VIEW_PATH_INC . "footer.html");

            }
        function moneda(){
                //echo "<script type='text/javascript'>alert('Hasta aqui llega');</script>";
                //$callback="index.php?module=home&view=home";
                //die('<script>window.location.href="'.$callback .'";</script>');

                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_HOME . 'list_details.html');

                require_once(VIEW_PATH_INC . "footer.html");

            }
        function shop(){
                //echo "<script type='text/javascript'>alert('Hasta aqui llega');</script>";
                //$callback="index.php?module=home&view=home";
                //die('<script>window.location.href="'.$callback .'";</script>');

                require_once(VIEW_PATH_INC . "header.php");
                require_once(VIEW_PATH_INC . "menu.php");

                loadView(VIEW_HOME . 'shop.html');

                require_once(VIEW_PATH_INC . "footer.html");

            }

    	function get_categories(){
    			$categories = loadModel(MODEL_HOME, "home_model", "get_categories");
    			echo json_encode($categories);
    			die();
    		}
    	function save_class(){
    			$_SESSION['clase']=$_POST['clase'];
    			die();
    		}
    	function get_class(){
    			echo json_encode($_SESSION['clase']);
    			die();
    		}
    	function get_monedas(){
    			$monedas = loadModel(MODEL_HOME, "home_model", "get_monedas", $_POST);
    			echo json_encode($monedas);
    			die();
    		}
        function get_all_monedas(){
                $monedas = loadModel(MODEL_HOME, "home_model", "get_all_monedas", $_POST);
                echo json_encode($monedas);
                die();
            }
    	function save_moneda(){
    			$_SESSION['moneda']=$_POST['data'];
    			die();
    		}
    	function get_moneda(){
    			echo json_encode($_SESSION['moneda']);
    			die();
    		}
        function get_monedas_by_cat(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "get_monedas_by_cat", $_POST['data']);
                echo json_encode($busqueda);
                die();
            }
            
        function get_busqueda(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "get_busqueda", $_POST['busqueda']);
                echo json_encode($busqueda);
                die();
            }
        function get_moneda_by_name(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "get_moneda", $_POST['busqueda']);
                echo json_encode($busqueda);
                die();
            }
        function popularity_categories(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "popularity_cat", $_POST['clase']);
                print_r($busqueda);
                die();
            }
        function popularity_crypto(){
                $busqueda = loadModel(MODEL_HOME, "home_model", "popularity_mon", $_POST['nombre']);
                print_r($busqueda);
                die();
            }
        function num_pages(){
                $item_per_page = 4;
                try {
                    //throw new Exception();
                    $total_monedas = loadModel(MODEL_HOME, "home_model", "count_total_mon_by_cat", $_POST['clase']);
                    $get_total_rows = $total_monedas[0]["total"]; //total records
                    $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
                } catch (Exception $e) {
                    showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                }

                if ($get_total_rows) { 
                    $jsondata["pages"] = $pages;
                    echo json_encode($jsondata);
                    exit;
                }
            }
        function num_pages_all(){
                $item_per_page = 4;
                try {
                    //throw new Exception();
                    $total_monedas = loadModel(MODEL_HOME, "home_model", "count_total_mon");
                    $get_total_rows = $total_monedas[0]["total"]; //total records
                    $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
                } catch (Exception $e) {
                    showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                }

                if ($get_total_rows) { 
                    $jsondata["pages"] = $pages;
                    echo json_encode($jsondata);
                    exit;
                }
            }
    }