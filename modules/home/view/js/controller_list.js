	$(document).ready(function() {
		$.ajax({
	    	type: "POST",
	    	url: "../../home/get_class",
	    	success: function (data) {
	    		var clase = JSON.parse(data);
			    $.ajax({
			    	type: "POST",
			    	url: "../../home/num_pages",
			    	data: {clase},
			    	success: function (data) {
			    		var data = JSON.parse(data);
			    		var pages = data.pages;
			    		var num = 1;
			    		$.ajax({
					    	type: "POST",
					    	url: "../../home/get_monedas",
					    	data: {clase,num},
					    	success: function (data) {
					      		var monedas = JSON.parse(data);
					      		$.each(monedas, function (i, valor) {
					                var monedas = "<div id='moneda'><input type='image' class='"+valor.nombre+"' onclick='get_data(this)' src='../../view/img/bitcoin_icono.jpg' id='img_cat' height='200px' width='200px'><p id='text_cat' onclick='get_data(this)' class='"+valor.nombre+"'>"+valor.nombre_web+"</p></div>";
					                $("#insert_monedas").append(monedas);
					            });

					    		$("#boot_page").bootpag({
						            total: pages,
						            page: 1,
						            maxVisible: 4,
						            next: 'next',
						            prev: 'prev'
						        }).on("page", function (e, num) {
						            //alert(num);
						            //e.preventDefault();
						            //$("#results").load("modules/products/controller/controller_products.class.php", {'page_num': num});
						      		$.ajax({
								    	type: "POST",
								    	url: "../../home/get_monedas",
								    	data: {clase,num},
								    	success: function (data) {
								    		
								      		var monedas = JSON.parse(data);
								      		while(document.getElementById('moneda')){
								      			document.getElementById('moneda').remove();
								      		}
								      		
								      		$.each(monedas, function (i, valor) {
								                var monedas = "<div id='moneda'><input type='image' class='"+valor.nombre+"' onclick='get_data(this)' src='../../view/img/bitcoin_icono.jpg' id='img_cat' height='200px' width='200px'><p id='text_cat' onclick='get_data(this)' class='"+valor.nombre+"'>"+valor.nombre_web+"</p></div>";
								                $("#insert_monedas").append(monedas);
								            });
								        }
								    });
								});
							}
				    	});
			        }
			    });
            
      		/* (BUTTON BACK DISABLED FOR THE MOMENT)
			<div id="back"></div>

            var back = "<button id='volver' onclick='back()'>Click me back</button>";
            $("#back").append(back);
      		*/
	      	}
	    });

	    $('#busqueda_cripto').keyup(function(){
	        //Obtenemos el value del input
	        var busqueda = $(this).val();
		    $.ajax({
            type: "POST",
            url: "../../home/get_busqueda",
            data: {busqueda},
            success: function(data){

				var obj = JSON.parse(data);

	                $('#busqueda_cripto').fadeIn(1000).html(obj);

	                    var arr=[];
	                    $.each(obj, function(i, value) {
	                    	arr.push(value.nombre_web);
	                    });
	                    $('#busqueda_cripto').autocomplete({
	                    	source: arr
	                    });
				}
			});
		});

		$("#busqueda_img").click(function(){
        	var busqueda = document.getElementById('busqueda_cripto').value;
        	$.ajax({
            type: "POST",
            url: "../../home/get_moneda_by_name",
            data: {busqueda},
            success: function(data){
				var obj = JSON.parse(data);
				var nombre = obj[0].nombre;
				get_data_v2(nombre);
				}
			});
	    });

	}); /* END READY */

	/* A traves de click de una imagen */
		function get_data(datos){
			var clase =[datos];
	        var clase = (clase[0].className);
			$.ajax({
		    	type: "GET",
		    	url: "https://api.coinmarketcap.com/v1/ticker/"+clase,
		    	success: function (data) {
		    		$.ajax({
				    	type: "POST",
				    	url: "../../home/save_moneda",
				    	data: {data},
				    	success: function (data) {
				    		window.location.replace("../moneda/");
				    		/* index.php?module=home&view=list_details */
				      	},
				      	error: function(){
				      		alert("Ha surgido un error al guardar los datos de la moneda");
				      	}
				    });
		      	}
		    });
		}
		function back(){
        	window.location.replace("../../home/index/");
        	/* index.php?module=home&view=home */
        }

    /* A traves del buscador */
	    function get_data_v2(nombre){
			$.ajax({
		    	type: "GET",
		    	url: "https://api.coinmarketcap.com/v1/ticker/"+nombre,
		    	success: function (data) {
		    		$.ajax({
				    	type: "POST",
				    	url: "../../home/save_moneda",
				    	data: {data},
				    	success: function (data) {
				    		window.location.replace("../../home/");
				    		/* index.php?module=home&view=list_details */
				      	},
				      	error: function(){
				      		alert("Ha surgido un error al guardar los datos de la moneda");
				      	}
				    });
		      	}
		    });
		}

function initMap(){
	$.ajax({
    	type: "POST",
    	url: "../../home/get_class",
    	success: function (data) {
    		var data = JSON.parse(data);
    		console.log(data);
    		$.ajax({
		    	type: "POST",
		    	url: "../../home/get_monedas_by_cat",
		    	data: {data},
		    	success: function (data) {

		    		var data = JSON.parse(data);
		    		var locations = [];

		    		$.each(data, function (i, valor) {
		    			var data = [];
		                data.push(valor.nombre_web);
		                data.push(valor.long);
		                data.push(valor.lat);
		                locations.push(data);
		            });
	                var map = new google.maps.Map(document.getElementById('map'), {
				    	zoom: 2,
				    	center: new google.maps.LatLng(0,0),
				    	mapTypeId: google.maps.MapTypeId.ROADMAP
				    });

				    var infowindow = new google.maps.InfoWindow();
				    var marker, i;
				    for (i = 0; i < locations.length; i++) {  
					    	marker = new google.maps.Marker({
					        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					        map: map
				    	});
				    google.maps.event.addListener(marker, 'click', (function(marker, i) {
				        return function() {
				    	    infowindow.setContent(locations[i][0]);
				        	infowindow.open(map, marker);
				        }
				      })(marker, i));
				    }
		        }
			});

    	}

	});
}