		
$(document).ready(function() {
		$.ajax({
	    	type: "POST",
	    	url: "../../home/get_moneda",
	    	success: function (data) {
	    		try{
		    		var data = JSON.parse(data);
		    		var nombre = data[0].name;
		    		$.ajax({
		    			type: "POST",
		    			url: "../../home/popularity_crypto",
		    			data:{nombre},
		    			error: function(){
		    				alert("Error on add popularity");
		    			}
		    		});
		    		var usd=data[0].price_usd;
		    		var cap = data[0].market_cap_usd;
		    		var max_cap=data[0].max_supply;
		    		getpriceur().then(function(eur) {
		    			var prieur= usd*eur;
		    			var cap_eur=cap*eur;
		    			var max_eur=max_cap*eur;
			            var moneda ="<p id='name_moneda'>Name: "+data[0].name+"</p>"+
			            			"<p id='price_euro'>Price on: "+prieur.toFixed(4)+"€</p>"+
			            			"<p id='price_bitcoin'>Price on btc: "+data[0].price_btc+"</p>"+
			            			"<p id='market_cap'>Market capital: "+cap_eur.toFixed(2)+"€</p>"+
			            			"<p id='max_supply'>Max capital: "+max_eur.toFixed(2)+"€</p>"+
			            			"<p id='percent_change_7d'>Change on 7d: "+data[0].percent_change_7d+"%</p>"+
			            			"<p id='percent_change_24h'>Change on 24h: "+data[0].percent_change_24h+"%</p>"+
			            			"<p id='percent_change_1h'>Change on 1h: "+data[0].percent_change_1h+"%</p>"+
			            			"<p id='rank'>Rank on cryptocurrencies: "+data[0].rank+"</p>"+
			            			"<button id='volver' onclick='back()'>Click me back</button>"
			            			;
			            $("#insert_moneda").append(moneda);
			            if (max_eur===0)
		    				alert("Error to take max capital")
			        });
			    }catch(err){
			    	alert("Ha surgido un error al recoger los datos de la moneda");
			    }
		        
	      	},
	      	error: function(){
	      		alert("Ha surgido un error al recoger los datos de la moneda");
	      	}
	    });

	    $('#busqueda_cripto').keyup(function(){
	        //Obtenemos el value del input
	        var busqueda = $(this).val();
		    $.ajax({
            type: "POST",
            url: "../../home/get_busqueda",
            data: {busqueda},
            success: function(data){

				var obj = JSON.parse(data);

	                $('#busqueda_cripto').fadeIn(1000).html(obj);

	                    var arr=[];
	                    $.each(obj, function(i, value) {
	                    	arr.push(value.nombre_web);
	                    });
	                    $('#busqueda_cripto').autocomplete({
	                    	source: arr
	                    });
				}
			});
		});

		$("#busqueda_img").click(function(){
        	var busqueda = document.getElementById('busqueda_cripto').value;
        	$.ajax({
            type: "POST",
            url: "../../home/get_moneda_by_name",
            data: {busqueda},
            success: function(data){
				var obj = JSON.parse(data);
				var nombre = obj[0].nombre;
				get_data(nombre);
				}
			});
	    });

	});/* END READY */

        function getpriceur() {
            const promise = new Promise(function(resolve, reject) {
                $.ajax({
                type: "GET",
                url: "https://api.fixer.io/latest?base=USD",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                success: function(data){
                    var euro=data.rates.EUR;
                    resolve(euro);
                }
                });
            });
           return promise
        };

        function back(){
        	window.location.replace("../../home/index/");
        	/* index.php?module=home&view=list_result */
        }

	    function get_data(nombre){
			$.ajax({
		    	type: "GET",
		    	url: "https://api.coinmarketcap.com/v1/ticker/"+nombre,
		    	success: function (data) {
		    		$.ajax({
				    	type: "POST",
				    	url: "../../home/save_moneda",
				    	data: {data},
				    	success: function (data) {
				    		window.location.replace("../moneda/");
				    		/* index.php?module=home&view=list_details */
				      	},
				      	error: function(){
				      		alert("Ha surgido un error al guardar los datos de la moneda");
				      	}
				    });
		      	}
		    });
		}