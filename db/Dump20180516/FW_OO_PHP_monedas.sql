-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: FW_OO_PHP
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `monedas`
--

DROP TABLE IF EXISTS `monedas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monedas` (
  `nombre` varchar(100) NOT NULL,
  `generacion` int(11) DEFAULT NULL,
  `popularidad` int(11) DEFAULT NULL,
  `nombre_web` varchar(45) DEFAULT NULL,
  `long` varchar(100) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nombre`),
  KEY `fk_monedas_1_idx` (`generacion`),
  CONSTRAINT `fk_monedas_1` FOREIGN KEY (`generacion`) REFERENCES `categoria` (`cod_cat`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monedas`
--

LOCK TABLES `monedas` WRITE;
/*!40000 ALTER TABLE `monedas` DISABLE KEYS */;
INSERT INTO `monedas` VALUES ('0x',5,3,'0x','-29.16508683','-17.86643297'),('aelf',6,1,'Aelf','-6.12820746','-43.97374544'),('Aeternity',2,3,'Aeternity','-54.12110416','48.02301107'),('Aion',2,2,'Aion','54.02517826','-28.81225427'),('Ardor',5,2,'Ardor','-60.15636792','-155.53919699'),('Ark',4,1,'Ark','-46.6356384','12.675993'),('Augur',3,2,'Augur','53.44721675','-88.92355841'),('Basic-Attention-Token',5,1,'Basic Attention Token','18.0252302','-129.36404291'),('Binance-Coin',6,8,'Binance Coin','-37.38895661','-100.21889124'),('Bitcoin',1,29,'Bitcoin','81.70835023','93.82141223'),('Bitcoin-Cash',4,10,'Bitcoin Cash','37.40938714','28.31088469'),('Bitcoin-Diamond',5,4,'Bitcoin Diamond','-10.36979015','61.58976968'),('Bitcoin-Gold',3,6,'Bitcoin Gold','34.0196233','-150.99451358'),('Bitcoin-Private',3,5,'Bitcoin Private','11.87502005','64.43455657'),('BitShares',4,5,'BitShares','-15.7431313','-63.78146937'),('Bytecoin',2,5,'Bytecoin','79.96552488','126.34426538'),('Bytom',6,7,'Bytom','27.33660418','99.83049567'),('Cardano',1,15,'Cardano','-55.32774677','125.70512397'),('Dash',6,9,'Dash','38.87535864','155.76625108'),('Decred',6,4,'Decred','-6.41112086','-111.38724502'),('DigiByte',4,2,'DigiByte','22.19835142','-126.37676572'),('DigixDAO',5,5,'DigixDAO','26.09480537','-172.8201529'),('Dogecoin',6,5,'Dogecoin','43.20787844','73.98532266'),('EOS',6,10,'EOS','19.37645604','145.22557149'),('Ethereum',2,16,'Ethereum','-33.15104705','-27.18601084'),('Ethereum-Classic',5,8,'Ethereum Classic','-7.33038342','174.07210673'),('Golem',4,3,'Golem','-2.57411664','61.8455137'),('Hshare',2,1,'Hshare','36.82428472','-172.60439907'),('ICON',5,7,'ICON','-58.33860822','-112.93248473'),('IOStoken',6,3,'IOStoken','60.77446519','117.73576823'),('IOTA',4,9,'IOTA','-66.27148628','59.25374058'),('Komodo',1,2,'Komodo','46.07456531','76.85926321'),('KuCoin-Shares',1,1,'KuCoin Shares','24.69632623','-22.17674355'),('Lisk',4,7,'Lisk','75.72148464','-9.09419677'),('Litecoin',5,10,'Litecoin','-9.37900715','-6.68841309'),('Loopring',6,3,'Loopring','1.48122723','-37.75593133'),('Maker',3,4,'Maker','12.87061423','-63.78618547'),('Monero',5,13,'Monero','41.49217159','19.07144611'),('Nano',2,6,'Nano','-15.69844605','-79.10125759'),('NEM',1,8,'NEM','-12.57601503','-28.66650408'),('NEO',3,9,'NEO','22.49068881','102.8743605'),('OmiseGO',2,8,'OmiseGO','-5.01737951','80.43828337'),('Ontology',3,7,'Ontology','-78.23239424','-172.35997636'),('Populous',1,5,'Populous','58.50588184','162.95724173'),('RChain',4,4,'RChain','44.0868056','18.66265234'),('Ripple',3,11,'Ripple','-74.48245447','39.40850016'),('Siacoin',6,6,'Siacoin','58.79803147','139.45117637'),('Status',3,3,'Status','-52.96661487','147.15355585'),('Steem',4,7,'Steem','-18.86946055','-63.92404801'),('Stellar',2,16,'Stellar','-17.32194954','-103.25876884'),('Stratis',1,4,'Stratis','-6.85008754','31.67007779'),('Tether',3,8,'Tether','-42.94496174','-41.38113516'),('TRON',2,8,'TRON','8.3506966','123.38148442'),('VeChain',4,8,'VeChain','8.79817095','157.16592426'),('Verge',1,7,'Verge','-20.64698876','93.73269575'),('Waltonchain',3,1,'Waltonchain','7.89961695','94.01704454'),('Wanchain',5,6,'Wanchain','-10.43578543','66.78661453'),('Waves',2,4,'Waves','-20.81002376','-69.63297909'),('Zcash',1,6,'Zcash','40.58079138','-109.74815877'),('Zilliqa',1,3,'Zilliqa','1.73325075','17.9223463');
/*!40000 ALTER TABLE `monedas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-16  0:37:46
