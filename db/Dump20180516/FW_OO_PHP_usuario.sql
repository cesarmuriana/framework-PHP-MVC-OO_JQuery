-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: FW_OO_PHP
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` varchar(100) NOT NULL,
  `nameuser` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `referraluser` varchar(100) DEFAULT NULL,
  `date_reception` varchar(50) DEFAULT NULL,
  `date_expiration` varchar(50) DEFAULT NULL,
  `interesado` varchar(50) DEFAULT NULL,
  `sexo` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `userdesc` varchar(50) DEFAULT NULL,
  `img` varchar(200) DEFAULT NULL,
  `Bitcoin` varchar(45) DEFAULT NULL,
  `IOTA` varchar(45) DEFAULT NULL,
  `Litecoin` varchar(45) DEFAULT NULL,
  `Ethereum` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `token` varchar(600) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `activate` varchar(150) DEFAULT NULL,
  `datebirthay` varchar(45) DEFAULT NULL,
  UNIQUE KEY `idusuario` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('2nBXPKqBjtfNJbyZkxj0mqMmOZd2','solo un simple tio','cuenta google','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg',NULL,NULL,NULL,NULL,'solounsimpletio@gmail.com','06b06b9bcd9300bdfe2743090483139377f06783','','1','10/01/1999'),('admin','admin','admin','admin','admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','3b068392461d08567e39c9c80803dac5e6519afc','D0865C55832FF53B33ABC597586734582C88DD4331A1C45E233328491BB40C9B','1',NULL),('ASAN48BqjFYaKrjyMr4OnmFodEr1','Cesar _06','Cesar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg',NULL,NULL,NULL,NULL,'cesarmurian@gmail.com','cc0664509ce1f23ce375f8a0ec5230f9dd187c9b','','',NULL),('el_cesar06','el_cesar06','iasbd','iubasddiukyb','admin','20/04/2018','27/04/2018','1','Hombre','ES','36','asidbluaosidb','','1','0','0','0',NULL,'f05b8cdbd7300ccd2cd74d2728fc4705d568945f','D0865C55832FF53B33ABC597586734582C88DD4331A1C45E233328491BB40C9B','1',NULL),('el_cesar0606','el_cesar0606','el_cesar0606',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,'cesarmuriana1@gmail.com','78c9f060371e2d9847d255e302b5d32ca659dd56','D0865C55832FF53B33ABC597586734582C88DD4331A1C45E233328491BB40C9B','1',NULL),('el_cesarito','el_cesarito','Name user','surname user','admin','19/04/2018','27/04/2018','1','Hombre','ES','36','asdbuaosiuydbasoy','','1','1','1','1',NULL,'35cc6adc3099ede51131e1f38b26ac2880ac3858','D0865C55832FF53B33ABC597586734582C88DD4331A1C45E233328491BB40C9B',NULL,NULL),('FqZSCw0n6XWu3cMWfePRG1QfrK73','SMX Cesar Muriana','SMX Cesar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'https://lh5.googleusercontent.com/-lVQHDyg91eA/AAAAAAAAAAI/AAAAAAAAADI/JCErVPxn814/photo.jpg',NULL,NULL,NULL,NULL,'smx.cmurianag@iestacio.com','f7baa26c25d8b52d16133488d804acea468202c5','','1',NULL),('Hz1Q6S7RZ2cCKolSZCwxGGC7FmQ2','solounsimpletio','','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png',NULL,NULL,NULL,NULL,'','b441a928117aaaaf63903e5780e3f783f59b6e10','','','10/01/1999'),('P0ovXa0S39ULsRBWnRPUeOwgqEq2','cesar muriana goya','cesar','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg',NULL,NULL,NULL,NULL,'cesarmuriana@gmail.com','526eaaf7a4ec21f8fbd1373e68d4809e01679dc9',NULL,NULL,'null');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-16  0:37:46
