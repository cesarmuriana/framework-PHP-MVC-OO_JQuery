-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: FW_OO_PHP
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `provincia`
--

DROP TABLE IF EXISTS `provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincia` (
  `idprovincia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `provincia` varchar(50) NOT NULL,
  `provinciaseo` varchar(50) NOT NULL,
  `provincia3` char(3) DEFAULT NULL,
  PRIMARY KEY (`idprovincia`),
  UNIQUE KEY `provinciaseo` (`provinciaseo`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincia`
--

LOCK TABLES `provincia` WRITE;
/*!40000 ALTER TABLE `provincia` DISABLE KEYS */;
INSERT INTO `provincia` VALUES (1,'Álava','alava','ALV'),(2,'Castellón','castellon','CAS'),(3,'León','leon','LEO'),(4,'Salamanca','salamanca','SAL'),(5,'Albacete','albacete','ABC'),(6,'Ceuta','ceuta','CEU'),(7,'Lleida','lleida','LLE'),(8,'Segovia','segovia','SGV'),(9,'Alicante','alicante','ALA'),(10,'Ciudad Real','ciudad-real','CRE'),(11,'Lugo','lugo','LUG'),(12,'Sevilla','sevilla','SVL'),(13,'Almería','almeria','ALM'),(14,'Córdoba','cordoba','CBA'),(15,'Madrid','madrid','MAD'),(16,'Soria','soria','SOR'),(17,'Asturias','asturias','AST'),(18,'A Coruña','coruna','LCO'),(19,'Málaga','malaga','MAL'),(20,'Tarragona','tarragona','TRN'),(21,'Ávila','avila','AVL'),(22,'Cuenca','cuenca','CNC'),(23,'Melilla','melilla','MEL'),(24,'S.C. Tenerife','tenerife','SCT'),(25,'Badajoz','badajoz','BDJ'),(26,'Girona','girona','GIR'),(27,'Murcia','murcia','MUR'),(28,'Teruel','teruel','TER'),(29,'Baleares','baleares','BAL'),(30,'Granada','granada','GND'),(31,'Navarra','navarra','NVR'),(32,'Toledo','toledo','TOL'),(33,'Barcelona','barcelona','BCN'),(34,'Guadalajara','guadalajara','GLJ'),(35,'Ourense','ourense','OUR'),(36,'Valencia','valencia','VAL'),(37,'Burgos','burgos','BRG'),(38,'Guipúzcoa','guipuzcoa','GPZ'),(39,'Palencia','palencia','PAL'),(40,'Valladolid','valladolid','VLL'),(41,'Cáceres','caceres','CAC'),(42,'Huelva','huelva','HLV'),(43,'Las Palmas','palmas','LPA'),(44,'Vizcaya','vizcaya','VZC'),(45,'Cádiz','cadiz','CDZ'),(46,'Huesca','huesca','HSC'),(47,'Pontevedra','pontevedra','PNV'),(48,'Zamora','zamora','ZAM'),(49,'Cantabria','cantabria','CTB'),(50,'Jaén','jaen','JAE'),(51,'La Rioja','rioja','LRJ'),(52,'Zaragoza','zaragoza','ZAR');
/*!40000 ALTER TABLE `provincia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-16  0:37:46
